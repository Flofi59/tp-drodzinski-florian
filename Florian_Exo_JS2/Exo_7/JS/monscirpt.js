//chargement du DOM
$(document).ready(function () {
  var rowCount = $("table#myTable tr:last").index() + 1;
  //fonction qui ajoute une ligne en fin de tableau
  $("#btnAdd").click(function () {
    let numLigne = $("#tab1 tbody tr").length + 1;
    $("#tab1 tbody").append("<tr><td>" + numLigne + "</td></tr>");
    calculTotal();
  });

  //fonction qui supprime la dernière ligne du tableau
  $("#btnDelete").click(function () {
    $("#tab1 tbody tr:last").remove();
    calculTotal();
  });

  function calculTotal() {
    //faire le calcul de tous les <tr><td>XXXXXX</td></tr>
    let somme = 0;
    $("#tab1 tbody tr td").each(function (index) {
      let num = parseInt($(this).html());
      somme += num;
    });
    //afficher dans tfoot
    $("#tab1 tfoot tr td").html(somme);
  }

});
