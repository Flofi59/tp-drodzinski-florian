//chargement du DOM
$(document).ready(function(){
//texte de la Div maDiv1    
$("#maDiv1").html("Coucou de jQuery");
//le bouton fait appel a la fonction qui cache la div
$("#btn1").click(cacherLaDiv);
//fonction qui cache la div avec le click du btn1
function cacherLaDiv(){
    $("#maDiv1").hide("slow");
}
//le bouton fais appel a la fonction qui affiche la div et change son texte par la même occassion
$("#btn2").click(afficherLaDiv);
//fonction afficher et qui change le texte
function afficherLaDiv(){
    $("#maDiv1").show("slow");
    $("#maDiv1").html("Mon texte affiché grâce à jQuery");    
}
})
