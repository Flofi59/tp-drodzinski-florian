//chargement du DOM
$(document).ready(function () {
  function afficherIMC() {
    //déclration variable poids du sliderPoids
    var poids = $("#idSliderPoids").val();
    poids = Number(poids);
    //déclration variable taille du sliderTaille
    var taille = $("#idSliderTaille").val();
    taille = Number(taille);
    $("#textPoids").html(poids);
    $("#textTaille").html(taille);
    var imc = calculerIMC(poids, taille / 100);
    //afficher le résultat
    var texte = imc.toFixed(1);
    texte += " (" + interpreterIMC(imc) + ")";
    $("#textIMC").html(texte);
  }

  $("#idSliderPoids").on("input", function () {
    //appel de la fonction
    $("#textPoids").html($("#idSliderPoids").val());
    afficherIMC();
  });

  $("#idSliderTaille").on("input", function () {
    //appel de la fonction
    $("#textTaille").html($("#idSliderTaille").val());
    afficherIMC();
  });
  //fonction qui calcul en fonction de l'imc si vous etes en surpoids ou non
  function interpreterIMC(prmValImc) {
    var interpretation = "";

    if (prmValImc < 16.5) {
      interpretation = "dénutrition";
    } else if (prmValImc < 18.5) {
      interpretation = "maigreur";
    } else if (prmValImc < 25) {
      interpretation = "corpulence normale";
    } else if (prmValImc < 30) {
      interpretation = "surpoids";
    } else if (prmValImc < 35) {
      interpretation = "obésité modérée";
    } else if (prmValImc < 40) {
      interpretation = "obésité sévère";
    } else if (prmValImc > 40) {
      interpretation = "obésité morbide";
    }
    return interpretation;
  }
  //calcul de l'imc
  function calculerIMC(prmPoids, prmTaille) {
    //poids en kg et taille en mètre
    var valRetour = prmPoids / (prmTaille * prmTaille);
    return valRetour;
  }
});
