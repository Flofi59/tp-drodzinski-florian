//chargement du DOM
$(document).ready(function () {
  function afficherIMC() {
    //déclration variable poids du sliderPoids
    var poids = $("#idSliderPoids").val();
    poids = Number(poids);
    //déclration variable taille du sliderTaille
    var taille = $("#idSliderTaille").val();
    taille = Number(taille);
    $("#textPoids").html(poids);
    $("#textTaille").html(taille);
    var imc = calculerIMC(poids, taille / 100);
    //afficher le résultat
    var texte = imc.toFixed(1);
    var silhouette = interpreterIMC(imc);
    texte += " (" + silhouette + ")";
    $("#textIMC").html(texte);
    afficherBalance(imc);
    afficherSilhouette(silhouette);
  }

  $("#idSliderPoids").on("input", function () {
    //appel de la fonction
    $("#textPoids").html($("#idSliderPoids").val());
    afficherIMC();
  });

  $("#idSliderTaille").on("input", function () {
    //appel de la fonction
    $("#textTaille").html($("#idSliderTaille").val());
    afficherIMC();
  });
  //fonction qui calcul en fonction de l'imc si vous etes en surpoids ou non
  function interpreterIMC(prmValImc) {
    var interpretation = "";

    if (prmValImc < 16.5) {
      interpretation = "dénutrition";
    } else if (prmValImc < 18.5) {
      interpretation = "maigreur";
    } else if (prmValImc < 25) {
      interpretation = "corpulence normale";
    } else if (prmValImc < 30) {
      interpretation = "surpoids";
    } else if (prmValImc < 35) {
      interpretation = "obésité modérée";
    } else if (prmValImc < 40) {
      interpretation = "obésité sévère";
    } else if (prmValImc > 40) {
      interpretation = "obésité morbide";
    }
    return interpretation;
  }
  //calcul de l'imc
  function calculerIMC(prmPoids, prmTaille) {
    //poids en kg et taille en mètre
    var valRetour = prmPoids / (prmTaille * prmTaille);
    return valRetour;
  }
});

function afficherBalance(prmValImc) {
  if (prmValImc >= 10 && prmValImc <= 45) {
    //position du curseur entre imcMax et imcMin
    var deplacement = 8.571 * prmValImc - 85.71; //calcul le déplacement du curseur sur la balance
    $("#aiguille").css("left", deplacement + "px"); //déplace le curseur
  }
}

function afficherSilhouette(prmValImc) {
  //Fonction affichage de la silhouette
  var sexe = $("input[name=sexe]:checked").val(); //Lecture de la valeur du bouton
  if (sexe == "homme") {
    //Si homme est selectionné 
    $("#silhouette").css("background-image", "url(css/img/IMC-homme.jpg)"); //affichage de l'image de l'homme
  } else {
    //Si femme est selectionné
    $("#silhouette").css("background-image", "url(css/img/IMC-femme.jpg)"); //affichage de l'image de la femme
  }
  var decalage = "";
  if (prmValImc == "dénutrition") {
    decalage = 630;
  }
  if (prmValImc == "maigreur") {
    decalage = 525;
  }
  if (prmValImc == "corpulence normale") {
    decalage = 420;
  }
  if (prmValImc == "surpoids") {
    decalage = 315;
  }
  if (prmValImc == "obésité modérée") {
    decalage = 210;
  }
  if (prmValImc == "obésité sévère") {
    decalage = 105;
  }
  if (prmValImc == "obésité morbide") {
    decalage = 105;
  }
  $("#silhouette").css("background-position", decalage);
}
