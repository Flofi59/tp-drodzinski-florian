//chargement du DOM
$(document).ready(function () {
  $("#btnCalculImc").click(function () {
    //déclration variable poids changement des ',' en '.' pour moins d'erreur
    var poids = $("#idPoids").val();
    poids = poids.replace(",", ".");
    poids = Number(poids);
    //déclration variable taille changement des ',' en '.' pour moins d'erreur
    var taille = $("#idTaille").val();
    taille = taille.replace(",", ".");
    taille = Number(taille);
    //condition en if si 'poids' ou 'taille' n'est pas un nombre
    if (isNaN(poids) || isNaN(taille)) {
      //affiche un message d'erreur
      $("#textIMC").html("Saisie incorrecte");
    } else {
      var imc = calculerIMC(poids, taille / 100);
      //afficher le résultat
      var texte = imc.toFixed(1);
      texte += " (" + interpreterIMC(imc) + ")";
      $("#textIMC").html(texte);
    }
  });
  //fonction qui calcul en fonction de l'imc si vous etes en surpoids ou non
  function interpreterIMC(prmValImc) {
    var interpretation = "";

    if (prmValImc < 16.5) {
      interpretation = "dénutrition";
    } else if (prmValImc < 18.5) {
      interpretation = "maigreur";
    } else if (prmValImc < 25) {
      interpretation = "corpulence normale";
    } else if (prmValImc < 30) {
      interpretation = "surpoids";
    } else if (prmValImc < 35) {
      interpretation = "obésité modérée";
    } else if (prmValImc < 40) {
      interpretation = "obésité sévère";
    } else if (prmValImc > 40) {
      interpretation = "obésité morbide";
    }
    return interpretation;
  }
  //calcul de l'imc
  function calculerIMC(prmPoids, prmTaille) {
    //poids en kg et taille en mètre
    var valRetour = prmPoids / (prmTaille * prmTaille);
    return valRetour;
  }
});
