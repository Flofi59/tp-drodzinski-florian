//déclaration des variables
let euros = 1 ;                                                      
let dollars = 1.65 ;
let TAUX = 1.65 ;
let affichage = "";

//boucle while pour conversion euro dollard jusqua 16384 euros
while(euros <= 16384){
    affichage = affichage + euros + " euro(s) = " + dollars.toFixed(2) + " dollar(s)" + "\n";
    euros = euros*2 ;
    dollars = euros*TAUX ;
};
//affichage de toutes les conversions
alert(affichage) ;